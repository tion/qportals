#include <QtTest>

#include "../config.h"
#include "../include/CoordinateData.h"

class coordinateDataTest : public QObject
{
   Q_OBJECT

   private slots:
      void instantiation();
      void variousCoordinates();
      void variousGlyphcodes();
      void invalidCharacters();
};

void coordinateDataTest::instantiation()
{
   CoordinateData *a = new CoordinateData;
   CoordinateData b = CoordinateData();

   QCOMPARE(a -> isEmpty(), true);
   QCOMPARE(b.isEmpty(), true);
   QCOMPARE(*a == b, true);
}

void coordinateDataTest::variousCoordinates()
{
   /*
    * 0001:0001:0001:0001
    * 0000:0000:0000:0000
    * 0fff:00ff:0fff:02ff
    * 00ba:0022:0a11:0079
    * 044d:0083:0d56:007e
    * 0fff:00ff:0fff:03ff
    * 0001:0001:0001:0000
    * 00ef:00ab:fa11:0000
    * abcd:1234:efgh:0320
    * 033a:0020:0bf2:0100
    * 0fef:00fa:0aaa:0038
    * 0801:0080:0801:007d
    */
   CoordinateData a = CoordinateData();

   a.setCoordinates (QStringLiteral("0001:0001:0001:0001"), 5);
   a.convertToGlyph();
   QCOMPARE (a.isEmpty(), false);
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0001:0001:0001:0001").toUpper());
   QCOMPARE (a.getPortal(), int (5));
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("400182802802").toUpper());

   a.reset();
   a.setCoordinates (QStringLiteral("0000:0000:0000:0000"), 16);
   a.convertToGlyph();
   QCOMPARE (a.isEmpty(), false);
   QCOMPARE (a.getErrorInfo().size(), int (4));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0000:0000:0000:0000").toUpper());
   QCOMPARE (a.getPortal(), int (16));
   QCOMPARE (a.getGlyphCode().isEmpty(), true);

   a.reset();
   a.setCoordinates (QStringLiteral("0fff:00ff:0fff:02ff"), 1);
   a.convertToGlyph();
   QCOMPARE (a.isEmpty(), false);
   QCOMPARE (a.getErrorInfo().isEmpty(), true);
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0fff:00ff:0fff:02ff").toUpper());
   QCOMPARE (a.getPortal(), int (1));
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("02ff80800800").toUpper());

   a.setCoordinates (QStringLiteral("00ba:0022:0a11:0079"), 1);
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("00ba:0022:0a11:0079").toUpper());
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("0079A32128BB"));

   a.reset();
   a.setCoordinate (QStringLiteral("044d:0083:0d56:007e"));
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("044d:0083:0d56:007e").toUpper());
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("007E04557C4E"));

   a.reset();
   a.setCoordinate (QStringLiteral("0fff:00ff:0fff:03ff"));
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (1));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0fff:00ff:0fff:03ff").toUpper());
   QCOMPARE (a.showErrorInfo(), QStringLiteral ("[03FF]"));

   a.reset();
   a.setCoordinate (QStringLiteral("0001:0001:0001:0000"));
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (1));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0001:0001:0001:0000"));
   QCOMPARE (a.showErrorInfo(), QStringLiteral ("[0000]"));

   a.reset();
   a.setCoordinate (QStringLiteral("00ef:00ab:fa11:0000"));
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (2));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("00ef:00ab:fa11:0000").toUpper());
   QCOMPARE (a.showErrorInfo(), QStringLiteral ("[FA11] [0000]"));

   a.reset();
   a.setCoordinate (QStringLiteral("abcd:1234:efgh:0320"));
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (4));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("abcd:1234:efgh:0320").toUpper());
   QCOMPARE (a.showErrorInfo(), QStringLiteral ("[ABCD] [1234] [EFGH] [0320]"));

   a.reset();
   a.setCoordinate (QStringLiteral("033a:0020:0bf2:0100"));
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("0100A13F3B3B"));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("033a:0020:0bf2:0100").toUpper());

   a.setCoordinate (QStringLiteral("0fef:00fa:0aaa:0038"));
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("00387B2AB7F0"));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0fef:00fa:0aaa:0038").toUpper());

   a.setCoordinate (QStringLiteral("0801:0080:0801:007d"));
   a.convertToGlyph();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("007D01002002"));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0801:0080:0801:007D"));
}

void coordinateDataTest::invalidCharacters()
{
   CoordinateData b = CoordinateData();

   b.setCoordinates (QStringLiteral("FFFF:FFFF:FFFF:X2D1"), 12);
   b.convertToGlyph();
   QCOMPARE (b.getGlyphCode().isEmpty(), true);
   QCOMPARE (b.getErrorInfo().size(), int (4));

   b.reset();
   b.setCoordinates (QStringLiteral("0FFF:F11F:FFFF:X2D1"), 12);
   b.convertToGlyph();
   QCOMPARE (b.getGlyphCode().isEmpty(), true);
   QCOMPARE (b.getErrorInfo().size(), int (3));

   b.reset();
   b.setCoordinates (QStringLiteral("02"), 8);
   b.convertToGlyph();
   QCOMPARE (b.getGlyphCode().isEmpty(), true);
   QCOMPARE (b.getErrorInfo().size(), int (4));

   b.reset();
   b.setGlyphCode (QStringLiteral("xxbf81234567"));
   b.convertToCoordinate();
   QCOMPARE (b.getGlyphCode(), QStringLiteral ("xxbf81234567").toUpper());
   QCOMPARE (b.getErrorInfo().size(), int (3));
}

void coordinateDataTest::variousGlyphcodes()
{
   /*
    * 022293b98453
    * xxbf81234567
    * 123456789abc
    * f230456789ed
    * f00182802802
    * f2ff80800800
    * f2ff81801801
    */
   CoordinateData a = CoordinateData();

   a.setGlyphCode (QStringLiteral("022293b98453"));
   a.convertToCoordinate();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0C52:0012:0397:0222"));
   QCOMPARE (a.getPortal(), int(1));

   a.reset();
   a.setGlyphCode (QStringLiteral("xxbf81234567"));
   a.convertToCoordinate();
   QCOMPARE (a.getErrorInfo().isEmpty(), false);
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("xxbf81234567").toUpper());

   a.reset();
   a.setGlyphCode (QStringLiteral("123456789abc"));
   a.convertToCoordinate();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("02BB:00D5:0F88:0234"));
   QCOMPARE (a.getPortal(), int(2));

   a.reset();
   a.setGlyphCode (QStringLiteral("f230456789ed"));
   a.convertToCoordinate();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("01EC:00C4:0E77:0230"));
   QCOMPARE (a.getPortal(), int(16));

   a.reset();
   a.setGlyphCode (QStringLiteral("f00182802802"));
   a.convertToCoordinate();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0001:0001:0001:0001"));
   QCOMPARE (a.getPortal(), int(16));

   a.reset();
   a.setGlyphCode (QStringLiteral("f2ff80800800"));
   a.convertToCoordinate();
   QCOMPARE (a.getErrorInfo().size(), int (0));
   QCOMPARE (a.getCoordinate(), QStringLiteral ("0FFF:00FF:0FFF:02FF"));
   QCOMPARE (a.getPortal(), int(16));

   a.reset();
   a.setGlyphCode (QStringLiteral("f2ff81801801"));
   a.convertToCoordinate();
   QCOMPARE (a.getErrorInfo().size(), int (3));
   QCOMPARE (a.getGlyphCode(), QStringLiteral ("f2ff81801801").toUpper());
   QCOMPARE (a.getPortal(), int(1));
}

QTEST_MAIN (coordinateDataTest)
#include "qtest_coordinatedata.moc"
