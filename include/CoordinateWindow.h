//------------------------------------------------------------------
#ifndef __NMS_COORDINATE_WINDOW_H__
#define __NMS_COORDINATE_WINDOW_H__
//------------------------------------------------------------------
#include <QtWidgets>

#include "CoordinateData.h"
#include "PortalAddressPickerWindow.h"
#include "PresetWidget.h"
//------------------------------------------------------------------
/** \details GUI interfaces and implementations for coordinate data. */
namespace NMS {
//------------------------------------------------------------------
/** \addtogroup NMS
 *  \brief Coordinate widgets.
 *  \details Collection of convenience widgets to display and 
 *  manipulate coordinate data.
 *  @{
 */

/** \class CoordinateInputWidget
 *
 *  \brief Widget for the input of a coordinate data value.
 * 
 *  \details Allows modifying for both the 
 *  19-digit coordinate and the portal number.
 *
 *  To create a CoordinateInputWidget object:
 *
 *       NMS::CoordinateInputWidget example("0123:0045:0678:0091");
 *       example.show();
 * 
 *  \see is_valid_coordinate is_acceptable_coordinate
 */
class CoordinateInputWidget : public QWidget
{
   Q_OBJECT

   public:
      /** \details Default constructor.
       * 
       *  Initializes with the provided _coords.
       * 
       *  \param[in] _coords A valid 19-character coordinate to set 
       *  the coordinate value as
       *  \param[in] parent The parent widget to draw onto
       */
      explicit CoordinateInputWidget(const QString &_coords = QString(), QWidget *parent = 0);
      /** \details Default destructor */
      ~CoordinateInputWidget();

   private:
      QLineEdit *coordinateInput;
      QSpinBox *portalSelect;
      QRegularExpressionValidator *coordValidator;
};
//------------------------------------------------------------------
/** \class GlyphCodeInputWidget
 *
 *  \brief Widget for input of a glyph code.
 * 
 *  \details Allows modifying the 12-digit glyph code
 *
 *  To create a GlyphCodeInputWidget object:
 *
 *       NMS::GlyphCodeInputWidget example("0123456789ab");
 *       example.show();
 *
 *  \see is_valid_glyph_code is_acceptable_glyph_code
 */
class GlyphCodeInputWidget : public QWidget
{
   Q_OBJECT

   public:
      /** \details Default constructor.
       * 
       *  Initializes with the provided _code.
       * 
       *  \param[in] _code A valid 12-digit glyph code to set
       *  \param[in] parent The parent widget to draw onto
       */
      explicit GlyphCodeInputWidget(const QString &_code = QString(), QWidget *parent = 0);
      /** \details Default destructor */
      ~GlyphCodeInputWidget();

   private:
      QLineEdit *glyphcodeInput;
      QRegularExpressionValidator *glyphValidator;
};
//------------------------------------------------------------------
/** \class CoordinateWindow
 * 
 *  \brief Widget for manipulating any coordinate data.
 * 
 *  \details A CoordinateWindow is a QDialog consisting of four tabs:
 *
 *       1) NMS::CoordinateInputWidget
 *       2) NMS::GlyphCodeInputWidget
 *       3) NMS::PortalDialerWidget
 *       4) NMS::PresetWidget
 *
 *  Data from tabs #1-3 can be added to #4 as presets and given 
 *  descriptions; refer to NMS::CoordinateWindow::setPreset.
 *
 *  The QDialog buttons are "convert" and "reset". Provided the code 
 *  entered is valid, convert will translate the coordinate + portal 
 *  number into a glyph code, or via versa. The type of conversion is 
 *  determined by which tab is currently visible/active.
 *
 *  The reset option will clear the data fields: coordinate, portal 
 *  number, and glyph code. This functionality does not affect 
 *  presets. Consider NMS::CoordinateWindow::resetPresets for that.
 *
 *  To create a CoordinateWindow object:
 *
 *       NMS::CoordinateWindow example;
 *       example.show();
 *
 *  If created on the heap, the CoordinateWindow object must be 
 *  freed within the scope it was initialized.
 *
 *  \see NMS::CoordinateInputWidget NMS::GlyphCodeInputWidget NMS::PortalDialerWidget NMS::PresetWidget
 *  \see is_valid_coordinate is_valid_glyph_code
 */
class CoordinateWindow : public QDialog
{
   Q_OBJECT

   public:
      /** \details Default constructor.
       * 
       *  Initializes with the provided _code.
       * 
       *  The code type is determined by code length. That is, 
       *  any string value provided that is 19 characters
       *  is assumed to be a coordinate, while 12 characters
       *  is presumed a glyph code.
       *
       *  \param[in] _code A valid coordinate or glyph code 
       *  to use as starting value
       *  \param[in] parent The parent widget to draw onto
       */
      explicit CoordinateWindow(const QString &_code = QString(), QWidget *parent = 0);
      /** \details Default destructor */
      ~CoordinateWindow();

      /** \returns The current coordinates and portal as 
       *  a QStringList. The coordinates are stored at index 
       *  0, while the portal index 1.
       */
      QStringList getCoordinate() const;
      /** \returns The current glyph code */
      QString getGlyph() const;
      /** \returns The current glyph code from portal dialer */
      QString getGlyphs() const;

   signals:
      /** \details This signal is emitted with the current 
       *  conversion error information.
       * 
       *  \param[out] message A QString containing the 
       *  invalid portions of the coordinate or glyph code
       */
      void errorMessage(const QString &message);
      /** \details This signal is emitted with the current 
       *  coordinate data held by the widget.
       *
       *  \param[out] coordData The current coordinate data 
       *  values held by the widget as a CoordinateData 
       *  object
       */
      void saveData(const CoordinateData &coordData);
      /** \details This signal is emitted when a reset has 
       *  taken place.
       */
      void resetSignal();
      /** \details This signal is emitted when a valid 
       *  glyph code has been converted to or from.
       */
      void validCode(const QString &theCode);
      /** \details This signal is emitted when a new preset 
       *  is ready to be added.
       * 
       *  \param[out] newPresetData A valid, non-empty 
       *  CoordinateData
       *  \param[out] newPresetAbbr A non-empty QString to 
       *  quickly identify this preset; abbreviation for the 
       *  preset
       *  \param[out] newPresetInfo A non-empty QString describing 
       *  this preset
       *  \param[out] skipFirstEdit A bool determining whether 
       *  to allow prompting to set/change param preset information. 
       *  This value is false by default for presets added via 
       *  tabs, allowing new abbreviation/description to be set in a 
       *  more streamlined manner
       *
       *  \see PresetWidget::addPreset
       *
       */
      void addNewPreset(const QString &newPresetData, 
                     const QString &newPresetAbbr, 
                     const QString &newPresetInfo,
                     const bool skipFirstEdit = false);
      /** \details Loads preset coordinate data from a file.
       * 
       *  \param[out] _file The filename to read preset data from
       */
      void importPresetsSignal(const QString &_file);
      /** \details Saves preset coordinate data to a file.
       *
       *  \param[in] file A valid filename to read preset data 
       *  from
       * 
       *  \see NMS::PresetWidget::toFile
       */
      void exportPresetSignal(const QString &file);
      /** \details This signal is emitted when presets should 
       *  be set to empty state. This signal normally should not 
       *  be called.
       */
      void resetPresetsSignal();

   public slots:
      /** \details Collects all the current coordinate data 
       *  into a CoordinateData object.
       * 
       *  \returns A CoordinateData object with the current 
       *  values for coordinate, portal number, and glyph 
       *  code.
       */
      CoordinateData exportData();
      /** \details Sets coordinate or glyph code values for 
       *  corresponding widget.
       *
       *  \param[in] _data A valid CoordinateData object 
       *  with valid coordinate data.
       */
      void importData(const CoordinateData &_data);
      /** \details Imports preset coordinate data from a file.
       * 
       *  \param[in] _file A valid file containing preset data 
       *  in the proper format
       *
       *  \see NMS::PresetWidget::fromFile
       */
      void loadPresets(const QString &_file = QString());
      /** \details Emits the signal exportPresetSignal.
       * 
       *  \param[in] _file A valid filename to load preset 
       *  data from
       * 
       *  \see NMS::PresetWidget::toFile
       */
      void savePresets(const QString &_file = QString());
      /** \details Sets either coordinate + portal or glyph 
       *  code values for corresponding widget.
       * 
       *  \param[in] _code A QStringList of at most size 2 
       *  with values expected:
       *       size 1: valid glyph code
       *       size 2: valid coordinate, valid portal
       */
      void setData(const QStringList &_code);
      /** \details Sets the portal dialer glyph code with the 
       *  provided _code.
       *
       *  \param[in] _code A valid glyph code, or hexadecimal 
       *  sequence
       */
      void setGlyphs(const QString &_code);
      /** \details Sets coordinate data as a preset. The current 
       *  visible tab determines which portion of the coordinate 
       *  data is used for the preset (even though it doesn't 
       *  really matter in the end; glyph code will always 
       *  convert to the same coordinates and vice versa).
       */
      void setPreset();
      /** \details Resets all coordinate values to empty 
       *  state.
       */
      void clear();
      /** \details Emits the signal resetPresetsSignal. */
      void resetPresets();
      /** \details Displays a QMessageBox describing the error 
       *  in the code conversion process.
       * 
       *  \param[in] _message A valid QString containing the 
       *  invalid portions of the code to be converted
       */
      void showErrorMessage(const QString &_message);

   private:
      QTabWidget *tabWidget;
      QDialogButtonBox *buttonBox;
      NMS::PresetWidget *_presets;

      /** \details Finds the index of a child widget. Assumes valid 
       *  child widgets are declared within the same namespace as 
       *  this class, NMS. Also assumes there are no duplicate 
       *  widgets since only the index of the first widget_name 
       *  found is returned.
       * 
       *  It is recommended to use the full class name as the 
       *  input.
       * 
       *  \param[in] widget_name Accepts either the full class name 
       *  or a short descriptor: "coordinate", "glyph"
       * 
       *  \returns The index of the widget_name. If the widget is 
       *  not found, -1 is returned
       */
      int indexOf(const QString &widget_name) const;
      /** \overload indexOf(const char *widget_name) const
       * 
       *  \param[in] widget_name Accepts either the full class name 
       *  or a short descriptor: "coordinate", "glyph"
       * 
       *  \returns The index of the widget_name. If the widget is 
       *  not found, -1 is returned
       */
      int indexOf(const char *widget_name) const;
      /** \details Sets coordinates and portal in 
       *  corresponding widget.
       * 
       *  \param[in] _code A valid coordinate
       *  \param[in] _portal A valid portal number
       */
      void setCoordinate(const QString &_code, const int _portal);
      /** \details Sets glyph code value in corresponding 
       *  widget.
       * 
       *  \param[in] _code A valid glyph code
       */
      void setGlyphCode(const QString &_code);

   private slots:
      /** \details Handles converting the coordinate data between 
       *  types.
       * 
       *  Performs the conversion and applies changes  
       *  to the corresponding widget:
       * 
       *    coordinate + portal => glyph code
       *    glyph code => coordinate + portal
       * 
       *  The type of conversion is determined by the 
       *  visible widget. For example, if the coordinate widget 
       *  is visible at invocation, then coordinate -> glyph 
       *  is attempted.
       */
      void convert();
};
//------------------------------------------------------------------
}  // end of namespace: NMS
/** @} End of doxygen group NMS */
//------------------------------------------------------------------
#endif   // __NMS_COORDINATE_WINDOW_H__
//------------------------------------------------------------------
