//------------------------------------------------------------------
#ifndef __NMS_COORDINATE_DATA_H__
#define __NMS_COORDINATE_DATA_H__
//------------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <sstream>
#include <QtCore>
//------------------------------------------------------------------
/** \addtogroup Helpers
 *  \brief Helper class and functions for manipulating coordinates.
 *  \details These are helper objects and functions for converting 
 *  coordinates to glyph codes and glyph codes to coordinates.
 *
 *  CoordinateData is a wrapper class for 
 *  interacting with the coordinate helper functions. All operations 
 *  performed by the class can be done manually. The benefit to using 
 *  the class is that it requires less repeated conversions to obtain 
 *  extra information, as this data is stored within the class. Using 
 *  the class is overkill for one-shot conversions, and is best 
 *  suited for uses that require repeated conversions over time or 
 *  preserving many coordinates.
 *
 *  For example:
 *
 *       // With CoordinateData
 *       CoordinateData a("0123:0045:0678:0199", 6);
 *       a.convertToGlyph();
 *       std::cout << a;
 *
 *       // Manually
 *       std::cout << "Glyph code: ";
 *       std::cout << convert_coordinate_to_glyph_code("0123:0045:0678:0199", 6).toStdString();
 *
 *  @{
 */

/** \class CoordinateData
 *
 *  \brief Holds data about a coordinate and its glyph code.
 *
 *  \details A coordinate is comprised a 19-digit hexadecimal value,
 *  colon-separated every 4 digits. Its corresponding glyph code is
 *  a 12-digit hexadecimal value.
 *
 *  Valid coordinates exist between:
 *
 *      0001:0001:0001:0001 ... 0FFF:00FF:0FFF:02FF
 *
 *  Valid glyph codes exist between:
 *
 *      000180800800 ... F2FF82802802
 *      HHHH81801801 is invalid though!
 *
 *  To create a coordinate object:
 *
 *      CoordinateData example ("0123:0045:0678:02AB");
 *
 */
class CoordinateData
{
   public:
      /** \details Default constructor.
       *  
       *  Initializes with the provided _code.
       * 
       *  By default,
       *  a coordinate is assumed to be provided as these are
       *  more common to encounter. However, the code type
       *  is determined by code length. That is, any string
       *  value provided that is 16 or 19 characters
       *  is assumed to be a coordinate, while 12 characters
       *  is presumed a glyph code.
       *
       *  \param[in] _code A valid coordinate or glyph code
       *  \param[in] _portal A valid portal number. This value 
       *  is not required, especially if a glyph code is 
       *  provided by _code
       */
      explicit CoordinateData(const QString &_code = QString(), const int _portal = int(1));
      /** \details Default destructor */
      ~CoordinateData();

      /** \details Copy constructor for const CoordinateData object
       *
       *  \param[in] other An initialized CoordinateData object
       *  to copy data from
       */
      CoordinateData(const CoordinateData &other);
      /** \overload CoordinateData(CoordinateData &other)
       * 
       *  \param[in] other An initialized CoordinateData object
       *  to copy data from
       */
      CoordinateData(CoordinateData &other);

      /** \details Assignment operator for const CoordinateData object
       *
       *  \param[in] source An initialized CoordinateData object
       *  to copy data from
       * 
       *  \returns This object with the data from source
       */
      CoordinateData &operator = (const CoordinateData &source);
      /** \details Assignment operator for const CoordinateData 
       *  object.
       * 
       *  \param[in] source An initialized CoordinateData object
       *  to copy data from
       * 
       *  \returns This object with the data from source
       */
      CoordinateData &operator = (CoordinateData &source);

      /** \returns The glyph code */
      QString getGlyphCode() const;
      /** \returns The coordinates */
      QString getCoordinate() const;
      /** \see getCoordinate */
      QString getCoordinates() const;
      /** \returns The portal number */
      int getPortal() const;
      /** \returns Error information about the code */
      QStringList getErrorInfo() const;
      /** \returns Error information about incorrect portions 
       *  of the coordinate/glyph code. If no errors, then an 
       *  empty QString is returned.
       */
      QString showErrorInfo() const;
      /** \details Returns the coordinates data in a 
       *  human-readable format. Collects all the coordinate 
       *  data (coordinate, portal number, and glyph code) 
       *  into a QString.
       *
       *  \returns A QString of the coordinate values.
       */
      QString toString() const;
      /** \details Determines whether the object is empty or has  
       *  NULL values.
       *
       *  \returns True, if the object has no coordinate data
       */
      bool isEmpty() const;
      /** \returns A copy of this object */
      CoordinateData data();
      /** \returns A copy of this object
       * 
       *  \see data
       */
      CoordinateData constData() const;

      /** \details Sets the coordinate value.
       *
       *  \param[in] _code A valid coordinate to set as
       * 
       *  \warning There is no error-checking of the coordinate 
       *  value to be set.
       *
       *  \see is_valid_coordinate
       */
      void setCoordinate(const QString &_code);
      /** \details A convenience function to set the coordinate 
       *  and portal value.
       * 
       *  \param[in] _code A valid coordinate to set as
       *  \param[in] _portal A valid portal to set as
       */
      void setCoordinates(const QString &_code, const int _portal);
      /** \details Sets the portal number associated with the 
       *  coordinate.
       *
       *  \param[in] _number A valid portal number to set as
       * 
       *  \warning There is no error-checking of the portal 
       *  value to be set.
       * 
       *  \see is_valid_portal
       */
      void setPortal(const int _number);
      /** \details Sets the glyph code.
       *
       *  \param[in] _code A valid glyph code to set as
       * 
       *  \warning There is no error-checking of the glyph 
       *  code to be set.
       * 
       *  \see is_valid_glyph_code
       */
      void setGlyphCode(const QString &_code);
      /** \details Adds error information about invalid coordinate
       *  and/or glyph code.
       *
       *  \param[in] error_data The text describing the problematic
       *  portions of the coordinate and/or glyph code
       */
      void addToErrorInfo(const QString &error_data);

      /** \details Accepts input, presumably from commandline or a 
       *  file, to directly assign coordinate values. Expects data 
       *  to be space-separated:
       *
       *  Ex:
       *
       *      <coordinate> <portal number> [<glyph code>]
       *         OR
       *      <glyph code>
       *
       *      // Examples of acceptable input format
       *      "0123:0045:0678:009A 2" 000202002002
       *      000202002002
       *      044d:0032:001c:0231 1
       *
       *      // Example of accepting input from a file
       *      CoordinateData example = CoordinateData();
       *      QFile testData ("whatissixteen.txt");
       *      if (testData.open (QFile::ReadOnly))
       *      {
       *         QTextStream input (&testData);
       *         input >> example;
       *         // do stuff with example;
       *      }
       *      testData.close();
       *
       *  \param[in,out] readInputInto The stream to read data from
       *  \param[in,out] target The object to read data into
       */
      friend QTextStream &operator >> (QTextStream &readInputInto,
                                       CoordinateData &target);
      /** \details Accepts input, presumably from commandline or a 
       *  file, to directly assign coordinate values. Expects data 
       *  to be space-separated:
       *
       *  Ex:
       *
       *      <coordinate> <portal number> [<glyph code>]
       *         OR
       *      <glyph code>
       *
       *      // Examples of acceptable input format
       *      "0123:0045:0678:009A 2" 000202002002
       *      000202002002
       *      044d:0032:001c:0231 1
       *
       *      // Example of accepting input from a file
       *      CoordinateData example = CoordinateData();
       *      std::ifstream testData ("whatissixteen.txt");
       *      if (testData.is_open())
       *      {
       *         testData >> example;
       *         // do stuff with example
       *      }
       *      testData.close();
       *
       *  \param[in,out] readInputFrom The stream to read data from
       *  \param[in,out] target The object to read data into
       */
      friend std::istream &operator >> (std::istream &readInputFrom, CoordinateData &target);
      /** \details Outputs the contents of CoordinateData object in a
       *  human-readable format.
       *
       *  \param[in,out] outs The stream to output data into.
       *  \param[in] the_data The object to read data from.
       * 
       *  \see toString
       */
      friend QTextStream &operator << (QTextStream &outs,
                                       const CoordinateData &the_data);
      /** \details Outputs the contents of CoordinateData object in a
       *  human-readable format.
       *
       *  \param[in,out] outs The stream to output data into.
       *  \param[in] the_data The object to read data from.
       * 
       *  \see toString
       */
      friend QTextStream &operator << (QTextStream &outs,
                                       CoordinateData &the_data);
      /** \details Allows output to QDebug stream. For example:
       *
       *       CoordinateData b;
       *       b.setCoordinate ("0123:0045:0678:009a");
       *       b.setPortal (8);
       *       b.convertToGlyph();
       *       qDebug() << b;
       * 
       *  Which would display something like:
       * 
       *       [DEBUG][CoordinateData]
       *       Coordinates: 0123:0045:0678:009A, portal #8
       *       Glyph code: 709AC6E79924
       *
       *  \param[in,out] dbg The QDebug stream to output data into.
       *  \param[in] the_data The object to read data from.
       */
      friend QDebug operator << (QDebug dbg,
                                 const CoordinateData &the_data);
      /** \details Allows output to QDebug stream. For example:
       * 
       *       CoordinateData b;
       *       b.setCoordinate ("0123:0045:0678:009a");
       *       b.setPortal (8);
       *       b.convertToGlyph();
       *       qDebug() << b;
       * 
       *  Which would display something like:
       * 
       *       [DEBUG][CoordinateData]
       *       Coordinates: 0123:0045:0678:009A, portal #8
       *       Glyph code: 709AC6E79924
       *
       *  \param[in,out] dbg The QDebug stream to output data into.
       *  \param[in] the_data The object to read data from.
       */
      friend QDebug operator << (QDebug dbg, CoordinateData &the_data);
      /** \details Outputs the contents of CoordinateData object in a
       *  human-readable format. Allows output to a  
       *  STL-compatible stream. 
       * 
       *  For example:
       *
       *       std::cout << CoordinateData ("0123:0045:0678:009a");
       * 
       *  \param[in,out] outs The stream to output data into.
       *  \param[in] the_data The object to read data from.
       * 
       *  \see toString
       */
      friend std::ostream &operator << (std::ostream &outs,
                                        const CoordinateData &the_data);
      /** \details Outputs the contents of CoordinateData object in a
       *  human-readable format. Allows output to a  
       *  STL-compatible stream. 
       * 
       *  For example:
       *
       *       std::cout << CoordinateData ("0123:0045:0678:009a");
       * 
       *  \param[in,out] outs The stream to output data into.
       *  \param[in] the_data The object to read data from.
       * 
       *  \see toString
       */
      friend std::ostream &operator << (std::ostream &outs,
                                        CoordinateData &the_data);

      /** \details Handles conversion of coordinate into a 
       *  glyph code.
       */
      void convertToGlyph();
      /** \details Handles conversion of a glyph code into a 
       *  coordinate.
       */
      void convertToCoordinate();
      /** \details Resets variable values to empty state. */
      void reset();

   private:
      /** \details Resets coordinate value to empty state. */
      void resetCoordinates();
      /** \details Resets glyph code value to empty state. */
      void resetGlyphCode();
      /** \details Resets error info value to empty state. */
      void resetErrorInfo();

      QString coordinate_data;
      QString glyphcode_data;
      QStringList errorinfo_data;
      int portal_data;
};
Q_DECLARE_METATYPE(CoordinateData);
//------------------------------------------------------------------
/*
 * NON-CLASS HELPER FUNCTIONS
 */
//------------------------------------------------------------------
/** \fn operator==(const CoordinateData &, const CoordinateData &)
 *
 *  \details Determines if two CoordinateData objects are the
 *  same.
 *
 *  \returns True, if a and b are equal.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator == (const CoordinateData &a, const CoordinateData &b);
/** \fn operator==(CoordinateData &, CoordinateData &)
 *
 *  \details Determines if two CoordinateData objects are the
 *  same.
 *
 *  \returns True, if a and b are equal.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator == (CoordinateData &a, CoordinateData &b);
/** \fn operator != (const CoordinateData &, const CoordinateData &)
 *
 *  \details Determines if two CoordinateData objects are not
 *  the same.
 *
 *  \returns True, if a and b are not equal.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator != (const CoordinateData &a, const CoordinateData &b);
/** \fn operator != (CoordinateData &, CoordinateData &)
 *
 *  \details Determines if two CoordinateData objects are not
 *  the same.
 *
 *  \returns True, if a and b are not equal.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator != (CoordinateData &a, CoordinateData &b);
/** \fn operator<(const CoordinateData &, const CoordinateData &)
 *
 *  \details Determines if a CoordinateData object is less than
 *  another CoordinateData object.
 *
 *  A coordinate is smallest the closer to 0x01 it is 
 *  (near the galactic center).
 *
 *  \returns True, if a is less than b.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator < (const CoordinateData &a, const CoordinateData &b);
/** \fn operator<(CoordinateData &, CoordinateData &)
 *
 *  \details Determines if a CoordinateData object is less than
 *  another CoordinateData object.
 *
 *  A coordinate is smallest the closer to 0x01 it is 
 *  (near the galactic center).
 *
 *  \returns True, if a is less than b.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator < (CoordinateData &a, CoordinateData &b);
/** \fn operator<=(const CoordinateData &, const CoordinateData &)
 *
 *  \details Determines if a CoordinateData object is less than or
 *  equal to another CoordinateData object.
 *
 *  A coordinate is smallest the closer to 0x01 it is 
 *  (near the galactic center).
 *
 *  \returns True, if a is less than or equal to b.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator <= (const CoordinateData &a, const CoordinateData &b);
/** \fn operator<=(CoordinateData &, CoordinateData &)
 *
 *  \details Determines if a CoordinateData object is less than or
 *  equal to another CoordinateData object.
 *
 *  A coordinate is smallest the closer to 0x01 it is 
 *  (near the galactic center).
 *
 *  \returns True, if a is less than or equal to b.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator <= (CoordinateData &a, CoordinateData &b);
/** \fn operator > (const CoordinateData &, const CoordinateData &)
 *
 *  \details Determines if a CoordinateData object is greater than
 *  another CoordinateData object.
 *
 *  A coordinate is largest the closer to 0xFFF it is 
 *  (edges of the galactic map).
 *
 *  \returns True, if a is less than b.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator > (const CoordinateData &a, const CoordinateData &b);
/** \fn operator > (CoordinateData &, CoordinateData &)
 *
 *  \details Determines if a CoordinateData object is greater than
 *  another CoordinateData object.
 *
 *  A coordinate is largest the closer to 0xFFF it is 
 *  (edges of the galactic map).
 *
 *  \returns True, if a is greater than b.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator > (CoordinateData &a, CoordinateData &b);
/** \fn operator >= (const CoordinateData &, const CoordinateData &)
 *
 *  \details Determines if a CoordinateData object is greater than
 *  or equal to another CoordinateData object.
 *
 *  A coordinate is largest the closer to 0xFFF it is 
 *  (edges of the galactic map).
 *
 *  \returns True, if a is greater than or equal to b.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator >= (const CoordinateData &a, const CoordinateData &b);
/** \fn operator >= (CoordinateData &, CoordinateData &)
 *
 *  \details Determines if a CoordinateData object is greater than
 *  or equal to another CoordinateData object.
 *
 *  A coordinate is largest the closer to 0xFFF it is 
 *  (edges of the galactic map).
 *
 *  \returns True, if a is greater than or equal to b.
 *
 *  \param[in] a An initialized CoordinateData object.
 *  \param[in] b An initialized CoordinateData object.
 */
bool operator >= (CoordinateData &a, CoordinateData &b);
//------------------------------------------------------------------
/** @} End of doxygen group Helpers */
//------------------------------------------------------------------
#endif   // __NMS_COORDINATE_DATA_H__
//------------------------------------------------------------------
