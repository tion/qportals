//------------------------------------------------------------------
#ifndef __NMS_PORTAL_DIALER_H__
#define __NMS_PORTAL_DIALER_H__
//------------------------------------------------------------------
#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QLineEdit>
//------------------------------------------------------------------
namespace NMS {
//------------------------------------------------------------------
/** \addtogroup NMS
 *  \brief Coordinate widgets.
 *  \details Collection of convenience widgets to display and 
 *  manipulate coordinate data.
 *  @{
 */

/** \class PortalDialerWidget
 *
 *  \brief Allows user to point-and-select a portal sequence, 
 *  similar to in-game portals.
 *
 *  \details Displays all possible glyphs and accepts a combination 
 *  of 12 glyphs for the user to choose as a glyph code.
 *
 *  To create a PortalDialerWidget object:
 *
 *       NMS::PortalDialerWidget example;
 *       example.setGlyphCode ("0023456789abc")    // Enter a specific glyph code sequence
 *       example.show();
 *
 *  If created on the heap, the PortalDialerWidget object must be freed 
 *  within the scope it is initialized.
 */
class PortalDialerWidget : public QWidget
{
   Q_OBJECT

   public:
      /** \details Default constructor.
       *
       *  Initializes with the specified _code.
       *
       *  \param[in] _code A valid hexadecimal number to set 
       *  as the glyph code
       *  \param[in] parent The parent window to draw onto
       */
      explicit PortalDialerWidget(const QString &_code = QString(), QWidget *parent = 0);
      /** \details Default destructor */
      ~PortalDialerWidget();

      /** \returns The current glyph code */
      QString getGlyphCode() const;

   signals:
      /** \details This signal is emitted when there is a 
       *  change to the glyph code.
       */
      void codeChanged();
      /** \details This signal is emitted when the glyph code 
       *  has or has not been filled to 12 hexadecimal digits. 
       *
       *  \param[out] ready A boolean value indicating code 
       *  completion status. False indicates glyph code is 
       *  incomplete.
       */
      void codeStatus(bool ready);

   public slots:
      /** \details Removes the last glyph entered, if any. */
      void removeLast();
      /** \details Resets all variables to empty state */
      void clear();
      /** \details Sets the glyph code along with its 
       *  corresponding glyph images.
       *
       *  \param[in] _code A valid glyph code
       */
      void setGlyphCode(const QString &_code);

   private:
      QVector<QToolButton *> glyph_select;
      QLineEdit *glyphcode_line;

   private slots:
      /** \details Selects the glyph image corresponding to 
       *  the index.
       *
       *  \param[in] index An integer between 0 and 15.
       */
      void addGlyphAtIndex(const int index);
      /** \details Disables or enables all glyph input. 
       *
       *  Intended to be used with codeStatus.
       * 
       *  \param[in] status A valid bool with the status of 
       *  currently entered glyph code
       */
      void toggleGlyphInput(bool status);
};
//------------------------------------------------------------------
}  // end of namespace: NMS
/** @} End of doxygen group NMS */
//------------------------------------------------------------------
#endif   // __NMS_PORTAL_DIALER_H__
//------------------------------------------------------------------
