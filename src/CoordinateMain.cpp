//==================================================================
#include <QCommandLineParser>
#include <QApplication>
#include <QTextStream>          // output text to screen/stdout
#include <QStyleFactory>

#include "MainWindow.h"
//==================================================================
void to_glyphs (const QString &code, const bool valid,
                QTextStream &outs, const bool verbosity)
{
   coordinateData *window = new coordinateData(code, valid);
   window -> convertToGlyph();

   if ( window -> getGlyphCode().isEmpty() == false )
      outs << window -> getGlyphCode() << "\n";
   else
   {
      if ( verbosity == true )
      {
         if ( window -> getErrorInfo().size() > 0 )
            outs << window -> showErrorInfo();
         else
         {
            if ( window -> getCoordinates().isEmpty() == false )
               outs << *window;
            else
               outs << "No coordinates provided.";
         }
      }
   }
}
//------------------------------------------------------------------
void to_coords (const QString &code, const bool valid,
                QTextStream &outs, const bool verbosity)
{
   coordinateData *guess = new coordinateData (code, valid);
   guess -> convertFromGlyph();

   if ( guess -> getCoordinate().isEmpty() == false )
   {
      outs << guess -> getCoordinate() << ", portal #";
      outs << guess -> getPortal() << "\n";
   }
   else
   {
      if ( verbosity == true )
      {
      	if ( guess -> getErrorInfo().size() > 0 )
      		outs << guess -> showErrorInfo();
      	else
      	{
            if ( guess -> getGlyphCode().isEmpty() == false )
               outs << *guess;
            else
               outs << "No glyph code provided.";
         }
      }
   }
}
//------------------------------------------------------------------
inline void setApplicationSettings()
{
   QCoreApplication::setOrganizationName ("tion");
   QCoreApplication::setApplicationName ("nmspd");
   QCoreApplication::setApplicationVersion ("0.6.1");
}
//------------------------------------------------------------------
int main (int argc, char *argv[])
{
   /*
     Use console-only if more than one (1) argument is given, or
     if the style option is not supplied (which is for GUI anyway)
   */
   if ( argc > 1 && argv[1] != QString("-style") )
   {
      // Use console/non-gui
      QCoreApplication app (argc, argv);
      setApplicationSettings();
      QCommandLineParser parser;
      parser.setApplicationDescription ("Converts coordinates to a portal glyph address, and vice versa. The GUI can be used instead when no options or arguments are specified.");
      parser.addHelpOption();
      parser.addVersionOption();
      parser.addPositionalArgument ("code",
                                    QCoreApplication::translate ("main",
                                          "Coordinate or glyph code sequence to convert."));

      // Boolean option with multiple names (-V, --verbose)
      QCommandLineOption verboseOption (QStringList() << "V" << "verbose",
                                        QCoreApplication::translate ("main",
                                              "Give more information about errors."));
      parser.addOption (verboseOption);

      // Boolean option with multiple names (-c, --coordinate)
      QCommandLineOption coordinateOption (QStringList() << "c" <<
                                           "coordinate",
                                           QCoreApplication::translate ("main",
                                                 "Convert from a star system coordinate. Must be in the form HHHH:HHHH:HHHH:HHHH (16 hex digits)"));
      parser.addOption (coordinateOption);

      // Boolean option with multiple names (-g, --glyphcode)
      QCommandLineOption glyphOption (QStringList() << "g" << "glyphcode",
                                      QCoreApplication::translate ("main",
                                            "Convert from a glyph code sequence. Must be in the form HHHHHHHHHHHH (12 hex digits)."));
      parser.addOption (glyphOption);

      // Process the actual command line arguments given by the user
      parser.process (app);

      // All arguments are lumped together...
      const QStringList args = parser.positionalArguments();

      bool coord = parser.isSet (coordinateOption);
      bool glyph = parser.isSet (glyphOption);
      bool verb = parser.isSet (verboseOption);
      QTextStream outs (stdout);

      if ( coord == true && glyph == false )
      {
         // convert coordinates -> glyph
         for (int i = 0; i < args.size(); i++)
         {
            to_glyphs (args.at(i), true, outs, verb);
         }
      }
      else if ( glyph == true && coord == false )
      {
         // convert glyph -> coordinates
         for (int i = 0; i < args.size(); i++)
         {
            to_coords (args.at(i), false, outs, verb);
         }
      }
      else
      {
         if ( args.size() == 0 )
         {
            outs << "Did you want to run the gui? ";
            outs << "Try again without any options.";
         }
         else
         {
            // Options are mixed so best guess the inputs
            for (int i = 0; i < args.size(); i++)
            {
               if ( args.at(i).size() == 12 )
               {
                  // Assume is glyph code
                  to_coords (args.at(i), false, outs, verb);
               }
               else if ( args.at(i).size() == 19 )
               {
                  // Assume is coordinate
                  to_glyphs (args.at(i), true, outs, verb);
               }
               else { /* Assume invalid input */ }
            }
         }
      }

      // Exit properly rather than waiting for input.
      app.quit();
   }
   else
   {
      QApplication app (argc, argv);
      setApplicationSettings();

      qDebug() << "Available options for \'-style\':" <<
                  QStyleFactory::keys().join(", ");

      MainWindow main_screen;
      main_screen.show();
      return app.exec ();
   }

   return (0);
}