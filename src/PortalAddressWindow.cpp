//------------------------------------------------------------------
#include <QtCore/QPointer>
#include <QPalette>                    // For glyph icons
#include <QIcon>                       // For glyph icons
#include <QtWidgets/QHBoxLayout>

#include "../include/PortalAddressWindow.h"
//------------------------------------------------------------------
const int GLYPHCODE_LENGTH = 12;
const int GLYPH_IMAGE_SIZE_IN_PX = 60;
//------------------------------------------------------------------
namespace NMS {
//------------------------------------------------------------------
PortalAddressWidget::PortalAddressWidget (const QString &_code, QWidget *parent)
   : QWidget (parent)
{
   portal_address.reserve (GLYPHCODE_LENGTH);

   for (int i = 0; i < GLYPHCODE_LENGTH; i++)
      portal_address.append (new QToolButton);

   for (int i = 0; i < GLYPHCODE_LENGTH; i++)
   {
      portal_address[i] -> setBaseSize (QSize (GLYPH_IMAGE_SIZE_IN_PX, GLYPH_IMAGE_SIZE_IN_PX));
      portal_address[i] -> setIconSize (QSize (GLYPH_IMAGE_SIZE_IN_PX, GLYPH_IMAGE_SIZE_IN_PX));
      portal_address[i] -> setBackgroundRole (QPalette::Shadow);
      portal_address[i] -> setCheckable (false);
      portal_address[i] -> setAutoRaise (true);
      portal_address[i] -> setText (QString ());
      portal_address[i] -> setCheckable (false);
   }

   // Import glyph code
   setGlyphCode (_code);

   QPointer<QHBoxLayout> sequence = new QHBoxLayout;
   sequence -> setSpacing (0);

   for (int i = 0; i < GLYPHCODE_LENGTH; i++)
      sequence -> addWidget (portal_address[i]);

   setLayout (sequence);
   setWindowTitle (QStringLiteral("Portal Address"));
}
//------------------------------------------------------------------
PortalAddressWidget::~PortalAddressWidget()
{
   // Pointer clean-up
   while (portal_address.count() > 0)
   {
      delete portal_address[0];
      portal_address.remove (0);
   }
}
//------------------------------------------------------------------
//////////////////
// PUBLIC SLOTS //
//////////////////
//------------------------------------------------------------------
void PortalAddressWidget::setGlyphCode (const QString &_code)
{
   clear();

   QString newGlyph = QString();

   for (int i = 0; i < GLYPHCODE_LENGTH; i++)
   {
      if (!newGlyph.isEmpty())
         newGlyph = QString();

      if (!_code.isEmpty())
         newGlyph = QStringLiteral(":/") + QString (_code.at (i)) + QStringLiteral(".jpg");
      else
         newGlyph = QStringLiteral(":/empty.png");

      portal_address[i] -> setIcon (QIcon (newGlyph));
   }
}
//------------------------------------------------------------------
void PortalAddressWidget::clear()
{
   for (int i = 0; i < GLYPHCODE_LENGTH; i++)
      portal_address[i] -> setIcon (QIcon (QStringLiteral(":/empty.png")));
}
//------------------------------------------------------------------
}  // end of namespace: NMS
