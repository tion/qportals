//------------------------------------------------------------------
#include <sstream>                  // std::stringstream
#include "../include/CoordinateData.h"
#include "../include/CoordinateDataHelpers.h"
//------------------------------------------------------------------
CoordinateData::CoordinateData (const QString &_code, const int _portal)
{
   coordinate_data = QString();
   glyphcode_data = QString();
   errorinfo_data = QStringList();
   portal_data = int(1);

   if (_code.isEmpty() == false)
   {
      // Import _code
      QString checkThisCode = _code.toUpper();

      if (checkThisCode.size() == 19)
      {
         coordinate_data = checkThisCode;

         if (is_valid_portal (_portal) == true && _portal > 1)
            portal_data = _portal;
      }
      else if (checkThisCode.size() == 12)
         glyphcode_data = checkThisCode;
      else
      {
         qDebug() << "[CoordinateData] Error instantiating with" << _code << "Using default value.";
      }
   }
}
//------------------------------------------------------------------
CoordinateData::CoordinateData (const CoordinateData &other)
{
   if (other.coordinate_data.isEmpty() == false)
      coordinate_data = other.coordinate_data;
   else
      coordinate_data = QString();

   if (is_valid_portal (other.portal_data) == true)
      portal_data = other.portal_data;
   else
      portal_data = int (1);

   if (other.glyphcode_data.isEmpty() == false)
      glyphcode_data = other.glyphcode_data;
   else
      glyphcode_data = QString();

   if (other.errorinfo_data.isEmpty() == false)
      errorinfo_data = other.errorinfo_data;
   else
      errorinfo_data = QStringList();
}
//------------------------------------------------------------------
CoordinateData::CoordinateData (CoordinateData &other)
{
   if (other.coordinate_data.isEmpty() == false)
      coordinate_data = other.coordinate_data;
   else
      coordinate_data = QString();

   if (is_valid_portal (other.portal_data) == true)
      portal_data = other.portal_data;
   else
      portal_data = int (1);

   if (other.glyphcode_data.isEmpty() == false)
      glyphcode_data = other.glyphcode_data;
   else
      glyphcode_data = QString();

   if (other.errorinfo_data.isEmpty() == false)
      errorinfo_data = other.errorinfo_data;
   else
      errorinfo_data = QStringList();
}
//------------------------------------------------------------------
CoordinateData &CoordinateData::operator = (const CoordinateData &source)
{
   // Handle self-assignment
   if (this != &source)
   {
      coordinate_data = source.coordinate_data;
      portal_data = source.portal_data;
      glyphcode_data = source.glyphcode_data;
      errorinfo_data = source.errorinfo_data;
   }

   return (*this);
}
//------------------------------------------------------------------
CoordinateData &CoordinateData::operator = (CoordinateData &source)
{
   // Handle self-assignment
   if (this != &source)
   {
      coordinate_data = source.coordinate_data;
      portal_data = source.portal_data;
      glyphcode_data = source.glyphcode_data;
      errorinfo_data = source.errorinfo_data;
   }

   return (*this);
}
//------------------------------------------------------------------
CoordinateData::~CoordinateData()
{
   coordinate_data.clear();
   glyphcode_data.clear();
   errorinfo_data.clear();
   portal_data = int(-1);
}
//------------------------------------------------------------------
void CoordinateData::reset()
{
   resetCoordinates();
   resetGlyphCode();
   resetErrorInfo();
}
//------------------------------------------------------------------
void CoordinateData::resetCoordinates()
{
   coordinate_data = QString();
   portal_data = 1;
}
//------------------------------------------------------------------
void CoordinateData::resetGlyphCode()
{
   glyphcode_data = QString();
}
//------------------------------------------------------------------
void CoordinateData::resetErrorInfo()
{
   errorinfo_data.clear();
}
//------------------------------------------------------------------
QString CoordinateData::getGlyphCode() const
{
   return (glyphcode_data);
}
//------------------------------------------------------------------
QString CoordinateData::getCoordinate() const
{
   return (coordinate_data);
}
//------------------------------------------------------------------
QString CoordinateData::getCoordinates() const
{
   return (getCoordinate());
}
//------------------------------------------------------------------
int CoordinateData::getPortal() const
{
   return (portal_data);
}
//------------------------------------------------------------------
QStringList CoordinateData::getErrorInfo() const
{
   return (errorinfo_data);
}
//------------------------------------------------------------------
QString CoordinateData::showErrorInfo() const
{
   QString result = QString();

   if (errorinfo_data.isEmpty() == false)
   {
      const QString info = QStringLiteral ("[") 
                                    + getErrorInfo().join ("] [")
                                    + QStringLiteral ("]");
      result.append (info);
   }

   return (result);
}
//------------------------------------------------------------------
bool CoordinateData::isEmpty() const
{
   bool result = false;

   if (coordinate_data.isEmpty() == true
         && glyphcode_data.isEmpty() == true)
   {
      result = true;
   }

   return (result);
}
//------------------------------------------------------------------
QString CoordinateData::toString() const
{
   QString result = QString();

   if ( (coordinate_data.isEmpty() == true
         && glyphcode_data.isEmpty() == true) ||
         (coordinate_data == QStringLiteral (":::")
          && glyphcode_data.isEmpty() == true))
   {
      result += QStringLiteral ("No coordinate data found.");
      result += QString ('\n');
   }
   else
   {
      if (coordinate_data.isEmpty() == false
            && coordinate_data != QStringLiteral (":::"))
      {
         result += coordinate_data;
         result += QStringLiteral (", portal #");
         result += QString::number (portal_data);
         result += QString ('\n');
      }

      if (glyphcode_data.isEmpty() == false)
      {
         result += glyphcode_data;
         result += QString ('\n');
      }
   }

   return (result);
}
//------------------------------------------------------------------
CoordinateData CoordinateData::data()
{
   return (CoordinateData (*this));
}
//------------------------------------------------------------------
CoordinateData CoordinateData::constData() const
{
   return (CoordinateData (*this));
}
//------------------------------------------------------------------
void CoordinateData::setCoordinate (const QString &_code)
{
   if (_code != coordinate_data
         || _code.toLower() != coordinate_data.toLower())
   {
      coordinate_data = _code.toUpper();
   }
}
//------------------------------------------------------------------
void CoordinateData::setCoordinates (const QString &_code, const int _portal)
{
   setCoordinate (_code);
   setPortal (_portal);
}
//------------------------------------------------------------------
void CoordinateData::setGlyphCode (const QString &_code)
{
   if (_code != glyphcode_data
         || _code.toLower() != glyphcode_data.toLower())
   {
      glyphcode_data = _code.toUpper();
   }
}
//------------------------------------------------------------------
void CoordinateData::setPortal (const int _number)
{
   if (_number != portal_data
         && is_valid_portal (_number) == true)
   {
      portal_data = _number;
   }
}
//------------------------------------------------------------------
void CoordinateData::addToErrorInfo (const QString &error_data)
{
   if (error_data.size() > 0
         && error_data.at (0) != ':')
   {
      errorinfo_data += error_data;
   }
}
//------------------------------------------------------------------
void CoordinateData::convertToGlyph()
{
   QStringList errorTrap = check_coordinate_for_errors (coordinate_data);

   // If no errors, continue with conversion
   if (errorTrap.isEmpty() == true)
   {
      QString temp = convert_coordinate_to_glyph_code (coordinate_data, portal_data);

      if (temp.isEmpty() == false)
         setGlyphCode (temp);
   }
   else
   {
      resetErrorInfo();

      for (int i = 0; i < errorTrap.size(); i++)
         addToErrorInfo (errorTrap.at (i));
   }
}
//------------------------------------------------------------------
void CoordinateData::convertToCoordinate()
{
   QStringList errorTrap = check_glyph_code_for_errors (glyphcode_data);

   if (errorTrap.isEmpty() == true)
   {
      QStringList _list = convert_glyph_code_to_coordinate (glyphcode_data);

      if (_list.size() == 2)
      {
         bool ok;
         setCoordinate (_list.at (0));
         setPortal (_list.at (1).toInt (&ok));
      }
      else
      {
         qDebug() << "[CoordinateData][convertToCoordinate] Error: returned list of size" << _list.size();
      }
   }
   else
   {
      resetErrorInfo();

      for (int i = 0; i < errorTrap.size(); i++)
         addToErrorInfo (errorTrap.at (i));
   }
}
//------------------------------------------------------------------
//////////////////////////
// COMPARISON OPERATORS //
//////////////////////////
//------------------------------------------------------------------
bool operator == (const CoordinateData &a, const CoordinateData &b)
{
   bool result = false;

   if (&a == &b)
      result = true;
   else
   {
      if (a.getCoordinate() == b.getCoordinate()
            && a.getGlyphCode() == b.getGlyphCode())
      {
         result = true;
      }
   }

   return (result);
}
//------------------------------------------------------------------
bool operator == (CoordinateData &a, CoordinateData &b)
{
   bool result = false;

   if (&a == &b)
      result = true;
   else
   {
      if (a.getCoordinate() == b.getCoordinate()
            && a.getGlyphCode() == b.getGlyphCode())
      {
         result = true;
      }
   }

   return (result);
}
//------------------------------------------------------------------
bool operator != (const CoordinateData &a, const CoordinateData &b)
{
   bool result = true;

   if (a == b)
      result = false;

   return (result);
}
//------------------------------------------------------------------
bool operator != (CoordinateData &a, CoordinateData &b)
{
   bool result = true;

   if (a == b)
      result = false;

   return (result);
}
//------------------------------------------------------------------
bool operator < (const CoordinateData &a, const CoordinateData &b)
{
   bool result = false;

   if (a != b)
   {
      if (a.getCoordinate() != b.getCoordinate())
      {
         if (a.getCoordinate() < b.getCoordinate())
            result = true;
      }
      else
      {
         if (a.getGlyphCode() < b.getGlyphCode())
            result = true;
      }
   }

   return (result);
}
//------------------------------------------------------------------
bool operator < (CoordinateData &a, CoordinateData &b)
{
   bool result = false;

   if (a != b)
   {
      if (a.getCoordinate() != b.getCoordinate())
      {
         if (a.getCoordinate() < b.getCoordinate())
            result = true;
      }
      else
      {
         if (a.getGlyphCode() < b.getGlyphCode())
            result = true;
      }
   }

   return (result);
}
//------------------------------------------------------------------
bool operator <= (const CoordinateData &a, const CoordinateData &b)
{
   bool result = false;

   if (a == b || a < b)
      result = true;

   return (result);
}
//------------------------------------------------------------------
bool operator <= (CoordinateData &a, CoordinateData &b)
{
   bool result = false;

   if (a == b || a < b)
      result = true;

   return (result);
}
//------------------------------------------------------------------
bool operator > (const CoordinateData &a, const CoordinateData &b)
{
   bool result = false;

   if (a != b)
   {
      if (a.getCoordinate() != b.getCoordinate())
      {
         if (a.getCoordinate() > b.getCoordinate())
            result = true;
      }
      else
      {
         if (a.getGlyphCode() > b.getGlyphCode())
            result = true;
      }
   }

   return (result);
}
//------------------------------------------------------------------
bool operator > (CoordinateData &a, CoordinateData &b)
{
   bool result = false;

   if (a != b)
   {
      if (a.getCoordinate() != b.getCoordinate())
      {
         if (a.getCoordinate() > b.getCoordinate())
            result = true;
      }
      else
      {
         if (a.getGlyphCode() > b.getGlyphCode())
            result = true;
      }
   }

   return (result);
}
//------------------------------------------------------------------
bool operator >= (const CoordinateData &a, const CoordinateData &b)
{
   bool result = false;

   if (a == b || a > b)
      result = true;

   return (result);
}
//------------------------------------------------------------------
bool operator >= (CoordinateData &a, CoordinateData &b)
{
   bool result = false;

   if (a == b || a > b)
      result = true;

   return (result);
}
//------------------------------------------------------------------
// FRIEND FUNCTIONS //
//------------------------------------------------------------------
QTextStream &operator >> (QTextStream &readInputInto, CoordinateData &target)
{
   QString line_of_data = QString();
   QString parseText = QString();

   readInputInto.readLineInto (&line_of_data);
   parseText = line_of_data.section (' ', 0, 0);

   if (parseText.size() == 19)
   {
      target.coordinate_data = parseText;
      bool ok;
      int portal_check = line_of_data.section (' ', 1, 1).toInt (&ok);

      if (is_valid_portal (portal_check) == true)
         target.portal_data = portal_check;
      else
         target.portal_data = int (1);

      parseText = line_of_data.section (' ', 2, 2);

      if (parseText.size() == 12)
         target.glyphcode_data = parseText;
   }
   else if (parseText.size() == 12)
      target.glyphcode_data = parseText;

   return (readInputInto);
}
//------------------------------------------------------------------
std::istream &operator >> (std::istream &readInputFrom, CoordinateData &target)
{
   std::string line_of_data = "";
   std::string parseText = "";
   std::stringstream ss;
   int _portal = 0;

   std::getline (readInputFrom, line_of_data);
   ss.str (line_of_data);
   ss << parseText;

   if (parseText.size() == 19)
   {
      target.coordinate_data = parseText.c_str();
      ss << _portal;

      if (is_valid_portal (_portal) == true)
         target.portal_data = _portal;
      else
         target.portal_data = int (1);

      ss << parseText;

      if (parseText.size() == 12)
         target.glyphcode_data = parseText.c_str();
   }
   else if (parseText.size() == 12)
      target.glyphcode_data = parseText.c_str();

   return (readInputFrom);
}
//------------------------------------------------------------------
QTextStream &operator << (QTextStream &outs, const CoordinateData &the_data)
{
   outs << the_data.toString();

   return (outs);
}
//------------------------------------------------------------------
QTextStream &operator << (QTextStream &outs, CoordinateData &the_data)
{
   outs << the_data.toString();

   return (outs);
}
//------------------------------------------------------------------
std::ostream &operator << (std::ostream &outs, const CoordinateData &the_data)
{
   outs << the_data.toString().toStdString();

   return (outs);
}
//------------------------------------------------------------------
std::ostream &operator << (std::ostream &outs, CoordinateData &the_data)
{
   outs << the_data.toString().toStdString();

   return (outs);
}
//------------------------------------------------------------------
QDebug operator << (QDebug dbg, const CoordinateData &the_data)
{
   QDebug outs = dbg.nospace();

   if ( (the_data.isEmpty() == true)
         || (the_data.coordinate_data == QStringLiteral (":::")
             && the_data.glyphcode_data.isEmpty() == true))
   {
      outs << "No coordinate data found.\n";
   }
   else
   {
      if (the_data.coordinate_data.size() > 0
            && the_data.coordinate_data != QStringLiteral(":::"))
      {
         outs << "\nCoordinates: ";
         outs.noquote() << the_data.coordinate_data.toUpper();
         outs << ", portal #" << the_data.portal_data << "\n";
      }

      if (the_data.glyphcode_data.isEmpty() == false)
      {
         outs << "Glyph code: ";
         outs.noquote() << the_data.glyphcode_data.toUpper();
      }

      if (the_data.errorinfo_data.isEmpty() == false)
      {
         outs << "\nInvalid segments: ";
         outs.noquote() << the_data.showErrorInfo();
      }
   }

   return (dbg);
}
//------------------------------------------------------------------
QDebug operator << (QDebug dbg, CoordinateData &the_data)
{
   QDebug outs = dbg.nospace();

   if ( (the_data.isEmpty() == true)
         || (the_data.coordinate_data == QStringLiteral (":::")
             && the_data.glyphcode_data.isEmpty() == true))
   {
      outs << "No coordinate data found.\n";
   }
   else
   {
      if (the_data.coordinate_data.size() > 0
            && the_data.coordinate_data != QStringLiteral (":::"))
      {
         outs << "\nCoordinates: ";
         outs.noquote() << the_data.coordinate_data.toUpper();
         outs << ", portal #" << the_data.portal_data << "\n";
      }

      if (the_data.glyphcode_data.isEmpty() == false)
      {
         outs << "Glyph code: ";
         outs.noquote() << the_data.glyphcode_data.toUpper();
      }

      if (the_data.errorinfo_data.isEmpty() == false)
      {
         outs << "\nInvalid segments: ";
         outs.noquote() << the_data.showErrorInfo();
      }
   }

   return (dbg);
}
//------------------------------------------------------------------
