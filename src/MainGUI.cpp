#include <QApplication>

#include "config.h"
#include "../include/MainWindow.h"

inline void setSettings()
{
   QCoreApplication::setOrganizationName (QString());
   QCoreApplication::setApplicationName (QStringLiteral (APPLICATION_NAME));
   QCoreApplication::setApplicationVersion (QStringLiteral(PROGRAM_VERSION));
   QLocale::setDefault(QLocale::c());
}

int main (int argc, char **argv)
{
   QApplication app (argc, argv);
   setSettings();
   CoordinateMainWindow a;
   a.show();

   return (app.exec());
}
