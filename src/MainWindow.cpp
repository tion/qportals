//------------------------------------------------------------------
#include "config.h"
#include "../include/MainWindow.h"
#include "../include/CoordinateDataHelpers.h"
//------------------------------------------------------------------
const int MESSAGE_DELAY_IN_MILLISECONDS = 2000;
const int LONGER_MESSAGE_DELAY_IN_MILLISECONDS = 10000;
//------------------------------------------------------------------
CoordinateMainWindow::CoordinateMainWindow (const CoordinateData &_code)
   : main_screen (new NMS::CoordinateWindow), 
   address_widget (new NMS::PortalAddressWidget)
{
   setCentralWidget (main_screen);
   setWindowTitle (QStringLiteral(APPLICATION_NAME));

   // Import coordinate data
   main_screen -> importData (_code);

   QPointer<QDockWidget> topWidget = new QDockWidget (QStringLiteral("Address"), this);
   topWidget -> setAllowedAreas (Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea);
   topWidget -> setFeatures (QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
   topWidget -> setWidget (address_widget);
   addDockWidget (Qt::TopDockWidgetArea, topWidget);

   /*
    * Create menu actions
    */
   exitAction = new QAction (QIcon::fromTheme (QStringLiteral("application-exit")), QStringLiteral("E&xit"));
   exitAction -> setShortcuts (QKeySequence::Quit);
   exitAction -> setStatusTip (QStringLiteral("Exit the application"));

   aboutAction = new QAction (QIcon::fromTheme (QStringLiteral("help-about")), QString (QStringLiteral("&About ") + QStringLiteral (APPLICATION_NAME)));
   aboutAction -> setStatusTip (QStringLiteral("Information about this application"));
   connect (aboutAction, &QAction::triggered, this, &CoordinateMainWindow::about);

   aboutQtAction = new QAction (QStringLiteral("About Qt"));
   aboutQtAction -> setStatusTip (QStringLiteral("Show information about the Qt library"));
   connect (aboutQtAction, &QAction::triggered, qApp, &QApplication::aboutQt);

   importAction = new QAction (QIcon::fromTheme (QStringLiteral("document-open")), QStringLiteral("&Import"));
   importAction -> setStatusTip (QStringLiteral("Import presets from a file"));
   connect (importAction, &QAction::triggered, this, [=]() { main_screen -> loadPresets(); });

   exportAction = new QAction (QIcon::fromTheme (QStringLiteral("document-save-as")), QStringLiteral("Ex&port"));
   exportAction -> setStatusTip (QStringLiteral("Save current presets to a file"));
   connect (exportAction, &QAction::triggered, this, [=]() { main_screen -> savePresets(); });

   freshStartAction = new QAction (QIcon::fromTheme(QStringLiteral("document-new")), QStringLiteral("&New"));
   freshStartAction -> setStatusTip (QStringLiteral("Start as new"));
   connect (freshStartAction, &QAction::triggered, this, [=]() { clear(); });

   preferenceMenuAction = new QAction (QStringLiteral("Preferences"));
   preferenceMenuAction -> setStatusTip(QStringLiteral("Adjust application options and preferences"));
   connect(preferenceMenuAction, &QAction::triggered, this, &CoordinateMainWindow::openPreferences);

   // Create status bar
   statusBar() -> showMessage (QStringLiteral ("Ready!"), MESSAGE_DELAY_IN_MILLISECONDS);

   /*
    * Create menus
    */
   fileMenu = menuBar() -> addMenu (QStringLiteral ("&File"));
   fileMenu -> addAction (freshStartAction);
   fileMenu -> addAction (importAction);
   fileMenu -> addAction (exportAction);
   fileMenu -> addAction (preferenceMenuAction);
   fileMenu -> addAction (exitAction);

   helpMenu = menuBar() -> addMenu (QStringLiteral ("&Help"));
   helpMenu -> addAction (aboutQtAction);
   helpMenu -> addAction (aboutAction);

   /*
    * Create toolbar
    */
   QPointer<QToolBar> mainToolBar = addToolBar (QStringLiteral ("Control"));
   mainToolBar -> setAllowedAreas (Qt::AllToolBarAreas);

   showPortalWindowAction = new QAction(QStringLiteral ("&Portal"));
   showPortalWindowAction -> setStatusTip (QStringLiteral ("Toggle portal address window"));
   connect (showPortalWindowAction, &QAction::triggered, this, &CoordinateMainWindow::showPortalWindow);
   mainToolBar -> addAction (showPortalWindowAction);
   mainToolBar -> addSeparator();

   showPresetWidgetAction = new QAction(QIcon::fromTheme (QStringLiteral("list-add")), QStringLiteral ("Add Pre&set"));
   showPresetWidgetAction -> setStatusTip (QStringLiteral ("Add current coordinate data as a preset"));
   connect (showPresetWidgetAction, &QAction::triggered, main_screen, &NMS::CoordinateWindow::setPreset);
   mainToolBar -> addAction (showPresetWidgetAction);
   mainToolBar -> addSeparator();

   /*
    * Create general actions
    */
   connect (main_screen, &NMS::CoordinateWindow::errorMessage, this, [=](const QString &msg)
   {
      statusBar() -> showMessage (msg, LONGER_MESSAGE_DELAY_IN_MILLISECONDS);
   });
   connect (main_screen, &NMS::CoordinateWindow::validCode, address_widget, &NMS::PortalAddressWidget::setGlyphCode);
   connect (main_screen, &NMS::CoordinateWindow::resetSignal, address_widget, &NMS::PortalAddressWidget::clear);
   connect (exitAction, &QAction::triggered, qApp, &QApplication::quit);
}
//------------------------------------------------------------------
CoordinateMainWindow::~CoordinateMainWindow()
{
   /*
    * Pointer clean-up
    */
   delete exitAction;
   delete aboutAction;
   delete aboutQtAction;
   delete importAction;
   delete exportAction;
   delete freshStartAction;
   delete preferenceMenuAction;
   delete showPortalWindowAction;
   delete showPresetWidgetAction;
   delete fileMenu;
   delete helpMenu;
   delete address_widget;
   delete main_screen;
}
//------------------------------------------------------------------
//////////////////
// PUBLIC SLOTS //
//////////////////
//------------------------------------------------------------------
void CoordinateMainWindow::showPortalWindow()
{
   if (address_widget -> isVisible())
      address_widget -> hide();
   else
      address_widget -> show();
}
//------------------------------------------------------------------
void CoordinateMainWindow::clear()
{
   main_screen -> clear();
   main_screen -> resetPresets();
   address_widget -> clear();
}
//------------------------------------------------------------------
void CoordinateMainWindow::importData (const CoordinateData &_data)
{
   main_screen -> importData (_data);
}
//------------------------------------------------------------------
void CoordinateMainWindow::setCoordinates (const QString &_coord, const int _portal)
{
   QStringList coords = QStringList();
   coords.append (_coord);
   coords.append (QString::number (_portal));
   main_screen -> setData (coords);
   coords.clear();
}
//------------------------------------------------------------------
void CoordinateMainWindow::setGlyphCode (const QString &_code)
{
   QStringList glyphs = QStringList();
   glyphs.append (_code);
   main_screen -> setData (glyphs);
   glyphs.clear();
}
//------------------------------------------------------------------
///////////////////
// PRIVATE SLOTS //
///////////////////
//------------------------------------------------------------------
void CoordinateMainWindow::openPreferences()
{
   // Load/open modal preferences window
   QPointer<QDialog>editPreferencesDialog = new QDialog(this);
   editPreferencesDialog -> setWhatsThis(QStringLiteral("Adjust application options"));
   editPreferencesDialog -> setModal(true);

   QPointer<QDialogButtonBox> editPreferencesButtons = new QDialogButtonBox(QDialogButtonBox::Ok
   | QDialogButtonBox::Cancel);

   QSettings application_settings(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + QDir::separator() + QStringLiteral("settings.conf"), QSettings::IniFormat);
   QPointer<QLineEdit> dbpathLine = new QLineEdit(application_settings.value("dbpath").toString());
   dbpathLine -> setWhatsThis(QStringLiteral("Choose the database file to save presets. Requires an application restart for changes to take effect."));
   dbpathLine -> setToolTip(dbpathLine -> whatsThis());

   QPushButton fileFinderButton;
   fileFinderButton.setWhatsThis(QStringLiteral("Navigate to file"));
   fileFinderButton.setCheckable(false);
   fileFinderButton.setIcon(QIcon::fromTheme(QStringLiteral("folder-open")));

   if (fileFinderButton.icon().isNull())
      fileFinderButton.setText(QStringLiteral("..."));

   QLabel dbfileLabel(QStringLiteral("Database path:"));

   QPointer<QFormLayout> editPreferencesLayout = new QFormLayout;
   editPreferencesLayout -> addRow (&fileFinderButton, dbpathLine);

   QPointer<QGroupBox> editPreferencesGroup = new QGroupBox;
   QPointer<QVBoxLayout> editPreferencesDBLayout = new QVBoxLayout;
   editPreferencesDBLayout -> addWidget (&dbfileLabel);
   editPreferencesDBLayout -> addLayout (editPreferencesLayout);
   editPreferencesGroup -> setLayout(editPreferencesDBLayout);

   QPointer<QVBoxLayout> editPreferencesMainLayout = new QVBoxLayout;
   editPreferencesMainLayout -> addWidget (editPreferencesGroup);
   editPreferencesMainLayout -> addWidget (editPreferencesButtons);

   editPreferencesDialog -> setWindowTitle (QStringLiteral("Preferences"));
   editPreferencesDialog -> setLayout (editPreferencesMainLayout);

   connect (editPreferencesButtons, &QDialogButtonBox::rejected, editPreferencesDialog, &QDialog::reject);
   connect (editPreferencesButtons, &QDialogButtonBox::accepted, editPreferencesDialog, &QDialog::accept);
   connect(&fileFinderButton, &QPushButton::clicked, this, [=]()
   {
      QString chosenFile = QString();
      QString path = QString();
      QStringList fileNames;

      if (dbpathLine -> text().isEmpty())
         path = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation).first();
      else
         path = dbpathLine -> text();

      QFileDialog file_dialog (this, QStringLiteral("Set DB File"), path, QStringLiteral("Preset files (*.presets);;Database files (*.db *.db3 *.sqlite3);;All files (*)"));
      file_dialog.setFileMode(QFileDialog::AnyFile);
      file_dialog.setViewMode (QFileDialog::List);

      if (file_dialog.exec())
      {
         fileNames = file_dialog.selectedFiles();

         if (fileNames.count() > 0)
            chosenFile = fileNames.at(0);
         else
            chosenFile = QString();
      }

      if (!chosenFile.isEmpty() && chosenFile != dbpathLine -> text())
         dbpathLine -> setText(chosenFile);
   });
   connect(editPreferencesDialog, &QDialog::finished, this, [=](int r)
   {
      if (r == QDialog::Accepted)
      {
         QSettings app_settings(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + QDir::separator() + QStringLiteral("settings.conf"), QSettings::IniFormat);

         if (dbpathLine -> text().isEmpty())
            setStatusTip(QStringLiteral("No change: empty text"));
         else if (dbpathLine -> text() == app_settings.value("dbpath").toString())
            setStatusTip(QStringLiteral("No change"));
         else
         {
            app_settings.setValue("dbpath", dbpathLine -> text());
            setStatusTip(QStringLiteral("Updated preferences"));
         }
      }
   });

   editPreferencesDialog -> exec();
}
//------------------------------------------------------------------
void CoordinateMainWindow::about()
{
   QMessageBox::about (this, QString (QStringLiteral("About ") + QStringLiteral (APPLICATION_NAME)),
                       QString (QStringLiteral("<center>") + QStringLiteral (PROGRAM_VERSION) + QStringLiteral("<br><br>") + 
                           QStringLiteral ("<a href=\"http://www.gnu.org/licenses/\" target=\"_blank\">") +
                           QStringLiteral ("GPL 3.0</a>") +
                           QStringLiteral ("</center>") +
                           QStringLiteral ("<hr />") +
                           QStringLiteral ("<p>") +
                           QStringLiteral ("The glyph symbols used in this program are slightly modified ") +
                           QStringLiteral ("from images on ") +
                           QStringLiteral ("<a href=\"https://nomanssky.gamepedia.com/Glyph\">") +
                           QStringLiteral ("https://nomanssky.gamepedia.com</a>. These images are ") +
                           QStringLiteral ("property of Hello Games.</p><hr />")));
}
//------------------------------------------------------------------
